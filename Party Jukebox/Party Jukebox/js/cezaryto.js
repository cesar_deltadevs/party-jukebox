﻿
var direccion = function () {
    //Obtiene el nombre del Servidor
    if (window.location.hostname == 'localhost') {
        return 'http://' + window.location.hostname + ':62244/';
    }
    else {
        if (window.location.protocol == "https") {
            return 'https://' + window.location.hostname + '/fiesta/';
        }
        else {
            return 'http://' + window.location.hostname + '/fiesta/';
        }
    }
};

function BuscarVideos(busqueda) {
    if (busqueda == "") {
        alert("No hay nada para buscar ¬¬");
    }
    else {
        $("#resultados").html("");

        $.ajax({
            type: 'POST', 
            url: direccion() + 'buscar', 
            data: { busqueda: busqueda }, 
            dataType: 'JSON'
        }).done(function (resultados) {
            if (resultados.resultado == 0) {
                //Formatea los Resultados
                var videos = JSON.parse(resultados.mensaje);
                console.log(videos.items);

                //Agrega los Videos al Resultado
                for (var v = 0; v < videos.items.length; v++) {
                    $("#resultados").append('<div class="row"><div class="col-sm-3"><img src="' + videos.items[v].snippet.thumbnails.medium.url + '" class="img-responsive img-video" /></div><div class="col-sm-7"><h5>' + videos.items[v].snippet.title + '</h5><p>' + videos.items[v].snippet.description + '</p></div><div class="col-sm-2"><button id="' + videos.items[v].id.videoId + '" class="btn btn-success btn-block agregar">Agregar</button></div></div><br />');
                }

                //agrega el Listener a los botones
                $(".agregar").click(function () {
                    AgregarVideo(this.id);
                });

                $("#busqueda").val("");
            }
            else {
                alert(resultados.mensaje);
            }
        }).fail(function (xhr, code, text) {
            alert("Error " + code + ': ' + text);
        });
    }
}

function AgregarVideo(video) {
    if (confirm("Deseas agregar este video a la Lista?")) {
        //$.ajax({
        //    type: "POST", 
        //    url: direccion() + "agregar",
        //    data: { video: video },
        //    dataType: 'text'
        //}).done(function (agregado) {
        //    if (agregado == "1") {
        //        alert("Video Agregado. Tu canción sonará pronto...");
        //        window.location.reload();
        //    }
        //    else {
        //        alert("Ocurrió un Error al Agregar el Video");
        //    }
        //}).fail(function (xhr, code, text) {
        //    alert("Error " + code + ': ' + text);
        //});

        window.location.href = direccion() + 'agregar?v=' + video;
    }
}


$(document).ready(function () {
    //Listener para el Boton de Buscar
    $("#buscar").click(function () {
        BuscarVideos($("#busqueda").val());
    });

    $("#busqueda").on("keyup", function (e) {
        if (e.keyCode == 13) {
            BuscarVideos($("#busqueda").val());
        }
    });
});
