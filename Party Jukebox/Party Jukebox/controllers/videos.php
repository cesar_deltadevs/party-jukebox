<?php

/**
 * videos short summary.
 *
 * videos description.
 *
 * @version 1.0
 * @author ccq
 */

namespace controllers {
    class Videos
    {

        private $API_key = "AIzaSyAfI_UJy63TNCRr4V13IQHm0VsuE1xDU88";
        private $CTE_key = "505796703575-an86al3iusvtogb3oijprpmt9qu1co17.apps.googleusercontent.com";
        private $SECRET_key = "KJcW4t80fzusleHNSRiMK798";

        public function Buscar($f3) {
            //Metodo para obtener los resultados los videos

            header('Access-Control-Allow-Origin: *');

            //Obtiene los parametros
            $terminos = $f3->get('POST.busqueda');

            if ($terminos != null) {

                $terminos2 = str_replace("-", "+", \Web::instance()->slug($terminos));
                $urlBuscar = "https://www.googleapis.com/youtube/v3/search?part=snippet&q=" . $terminos2 . "&type=videos&maxResults=50&key=" . $this->API_key;

                $resultados = file_get_contents($urlBuscar);

                exit(json_encode(
                    array('resultado' => 0, "mensaje" => $resultados)
                ));
            }
            else {
                exit(json_encode(
                    array('resultado' => -1, "mensaje" => "No hay palabras para buscar")
                ));
            }
        }

        public function Agregar($f3) {
            //Metodo para Agregar un Video a la Playlist
            //header('Access-Control-Allow-Origin: *');
            session_start();

            //Obtiene los parametros
            //$video = $f3->get('POST.video');
            $video = $f3->get('GET.v');

            if ($video != "") {
                require_once 'helpers/Google/autoload.php';
                //require_once 'helpers/Google/Service.php';
                //require_once 'helpers/Google/Client.php';
                //require_once 'helpers/Google/Service/YouTube.php';

                $cliente = new \Google_Client();
                $cliente->setClientId($this->CTE_key);
                $cliente->setClientSecret($this->SECRET_key);
                $cliente->setScopes("https://www.googleapis.com/auth/youtube");
                //$cliente->setScopes("https://www.googleapis.com/auth/youtube.readonly");
                $redirect = $f3->get('ruta') . "oauth2callback";
                //$redirect = "http://localhost:62244/oauth2callback";
                //$redirect = "http://cezaryto.com/oauth2callback";
                $cliente->setRedirectUri($redirect);
                $cliente->setAccessType("online");
                //$auth_url = $cliente->createAuthUrl();

                if (isset($_SESSION['code'])) {
                    //if (strval($_SESSION['state']) !== strval($_GET['state'])) {
                    //    //die('The session state did not match.');
                    //    die();
                    //}

                    $cliente->authenticate($_SESSION['code']);
                    $_SESSION['code'] = null;
                    $_SESSION['token'] = $cliente->getAccessToken();
                    //header('Location: ' . $redirect);
                }

                if (isset($_SESSION['token'])) {
                    $cliente->setAccessToken($_SESSION['token']);
                }

                // Check to ensure that the access token was successfully acquired.
                if ($cliente->getAccessToken()) {
                    try {

                        // Define an object that will be used to make all API requests.
                        $youtube = new \Google_Service_YouTube($cliente);

                        // Add a video to the playlist. First, define the resource being added
                        // to the playlist by setting its video ID and kind.
                        $resourceId = new \Google_Service_YouTube_ResourceId();
                        $resourceId->setVideoId($video);
                        $resourceId->setKind('youtube#video');

                        // Then define a snippet for the playlist item. Set the playlist item's
                        // title if you want to display a different value than the title of the
                        // video being added. Add the resource ID and the playlist ID retrieved
                        // in step 4 to the snippet as well.
                        $playListItemSnippet = new \Google_Service_YouTube_PlaylistItemSnippet();
                        $playListItemSnippet->setPlaylistId('PL3gylhp_pWsSD4_9p4Jhf2daIju3i-TWF');
                        $playListItemSnippet->setResourceId($resourceId);

                        // Finally, create a playlistItem resource and add the snippet to the
                        // resource, then call the playlistItems.insert method to add the playlist
                        // item.
                        $playListItem = new \Google_Service_YouTube_PlaylistItem();
                        $playListItem->setSnippet($playListItemSnippet);
                        $playListItemResponse = $youtube->playlistItems->insert('snippet,contentDetails', $playListItem, array());

                        var_dump($playListItemResponse);
                    }
                    catch (Google_Service_Exception $e) {
                        $htmlBody .= sprintf('<p>A service error occurred: <code>%s</code></p>',
                            htmlspecialchars($e->getMessage()));
                    }
                    catch (Google_Exception $e) {
                        $htmlBody .= sprintf('<p>An client error occurred: <code>%s</code></p>',
                            htmlspecialchars($e->getMessage()));
                    }

                    $_SESSION['token'] = $client->getAccessToken();
                } else {
                    // If the user hasn't authorized the app, initiate the OAuth flow
                    $state = mt_rand();
                    $cliente->setState($state);
                    $_SESSION['state'] = $state;

                    $authUrl = $cliente->createAuthUrl();
                    $htmlBody = "<h3>Authorization Required</h3>
                  <p>You need to <a href=" . $authUrl . ">authorize access</a> before proceeding.<p>";

                    echo $htmlBody;
                }
            }
            else {
                echo "<script>alert('No hay video para Agregar');window.location.href = '" . $f3->get('ruta') . "';</script>";
            }
        }
    }
}