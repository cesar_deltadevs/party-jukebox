﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Playlist Party</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <script src="<?= ($ruta) ?>js/jquery-3.3.1.js"></script>
    <script src="<?= ($ruta) ?>js/bootstrap.js"></script>
    <script src="<?= ($ruta) ?>js/cezaryto.js"></script>

    <link href="<?= ($ruta) ?>css/bootstrap.css" rel="stylesheet" />
    <link href="<?= ($ruta) ?>css/cezaryto.css" rel="stylesheet" />

</head>
<body class="container-fluid">
    <h1 class="text-center">Party Playlist</h1>
    
    <div class="row">
        <div class="col-md-10">
            <input type="text" class="form-control" placeholder="Buscar video..." id="busqueda" />
        </div>
        <div class="col-md-2">
            <button id="buscar" class="btn btn-primary btn-block">Buscar</button>
        </div>
    </div>
    <br />
    
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="text-center">Resultados</h3>
        </div>
        <div class="panel-body" id="resultados">
            <!-- Aqui se ponen los resultados -->
            <!--<div class="row">
                <div class="col-sm-3">
                    <img src="" class="img-responsive"  />
                </div>
                <div class="col-sm-7">
                    <h5>Titulo del Video</h5>
                    <p>Desripcion del video</p>
                </div>
                <div class="col-sm-2">
                    <button id="video_id" class="btn btn-success btn-block agregar">Agregar</button>
                </div>
            </div>-->
        </div>
    </div>
</body>
</html>
