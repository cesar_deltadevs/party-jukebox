<?php

// Inicializa el framework
$f3=require('lib/base.php');

$f3->set('DEBUG',1);
if ((float)PCRE_VERSION<7.9)
	trigger_error('PCRE version is out of date');

// Load configuration
$f3->config('config.ini');

//Configuracion de la ruta Global
//Produccion y local
if ($f3->get('DEBUG') > 0){
    //Obtiene la ruta del sitio si esta en DEBUG
    $f3->set('ruta', 'http://' . $f3->get('HOST') . ':' . $f3->get('PORT') . '/');
} else{
    $f3->set('ruta', 'http://' . $f3->get('HOST') . '/fiesta/');
}

// SECCION DE RUTAS #################################################################################################################

$f3->route('GET /', function($f3) {
    echo Template::instance()->render("busqueda.html");
});

$f3->route('POST /buscar', 'Controllers\Videos->Buscar');
//$f3->route('POST /agregar', 'Controllers\Videos->Agregar');
$f3->route('GET /agregar', 'Controllers\Videos->Agregar');

$f3->route('GET /oauth2callback', function ($f3) {
    if (isset($_GET['code'])) {
        session_start();
        $_SESSION['code'] = $_GET['code'];
        $f3->reroute('/');
    }
});

$f3->run();
